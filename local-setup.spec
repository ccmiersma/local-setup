%define author Christopher Miersma

Name:		local-setup
Version:        0.1.1
Release:        1.local%{?dist}
Summary:	Client-side setup/configuration and scripts for a Fedora Install
Group:		local
License:	MIT
URL:		https://gitlab.com/ccmiersma/local-setup/
Source0:	%{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  ruby
BuildRequires:  rubygems
BuildRequires:  make

%description
This provides client side Puppet modules and scripts for configuration.


%prep
%setup

# The actual work of configuring building and installing has been offloaded to 
# configure and make scripts. In this case, autotools are not used. The configure
# script uses ERB templates to generate the Makefile. The scripts are also generated from templates.
# 
%build

make

%install

%make_install


%clean
%__rm -rf %buildroot

%files
%defattr(-,root,root, -)
/etc/opt/local-setup
/var/opt/local-setup
/opt/local-setup
/etc/puppet/modules/setup

# The post and postun update the man page database
%post


%postun


%changelog
* Mon Nov 05 2018 Christopher Miersma <ccmiersma@gmail.com> 0.1.1-1.local
- new package built with tito


