package_name = local-setup
prefix = /opt/$(package_name)
datarootdir = $(prefix)/share
datadir = $(datarootdir)
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
sbindir = $(exec_prefix)/sbin
libexecdir = $(exec_prefix)/libexec
infodir = $(datarootdir)/info
sysconfdir = $(prefix)/etc
localstatedir = $(prefix)/var
docdir = $(datarootdir)/doc/$(package_name)
mandir = $(datarootdir)/man
libdir = $(prefix)/lib

modules := $(wildcard modules/*)

all: build

build:
	install -d -m 755 build/
	$(foreach module, $(modules), puppet module build $(module) && mv $(module)/pkg/*.tar.gz build/ && rm -rf $(module)/pkg)

.PHONY: check
check:
	make install DESTDIR=build/test

install: build README.md
	install -d -m 755 \
	       $(DESTDIR)/$(prefix) \
	       $(DESTDIR)/$(bindir) \
	       $(DESTDIR)/$(docdir) \
	       $(DESTDIR)/$(mandir) \
	       $(DESTDIR)/$(libdir) \
	       $(DESTDIR)/$(datarootdir)/modules \
	       $(DESTDIR)/etc/puppet/modules
	install -d -m 755 \
	       $(DESTDIR)/etc/$(prefix) \
	       $(DESTDIR)/var/$(prefix)
	ln -srT $(DESTDIR)/etc/$(prefix) $(DESTDIR)/$(sysconfdir)
	ln -srT $(DESTDIR)/var/$(prefix) $(DESTDIR)/$(localstatedir)
	install -m 644 README.md $(DESTDIR)/$(docdir)
	$(foreach module, $(wildcard build/*.tar.gz), \
		puppet module install \
		--verbose \
	       	--ignore-dependencies \
		--target-dir $(DESTDIR)/$(datarootdir)/modules \
		$(module))
	$(foreach module, $(modules), ln -srvt $(DESTDIR)/etc/puppet/modules $(DESTDIR)/$(datarootdir)/$(module))
	
make clean:
	rm -rf build/

